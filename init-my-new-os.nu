#!/home/lostpy/.cargo/bin/nu

def "success" [
  msg
  --newline (-n)
] {
  if $newline {
    print -n $"(ansi green)($msg)(ansi reset)"
  } else {
    print $"(ansi green)($msg)(ansi reset)"
  }
}

def "warning"  [
  msg
  --newline (-n)
] {
  if $newline {
    print -n $"(ansi yellow)($msg)(ansi reset)"
  } else {
    print $"(ansi yellow)($msg)(ansi reset)"
  }
}

def "bad" [
  msg
  --newline (-n)
] {
  if $newline {
    print -n $"(ansi red)($msg)(ansi reset)"
  } else {
    print $"(ansi red)($msg)(ansi reset)"
  }
}

def "info" [
  msg
  --newline (-n)
] {
  if $newline {
    print -n $"(ansi cyan)($msg)(ansi reset)"
  } else {
    print $"(ansi cyan)($msg)(ansi reset)"
  }
}

# Remove snap package
def "main remove snap" [] {
  try {
    systemctl stop snapd
  }
  try {
    apt remove --purge snapd
  }

  if ('/var/cache/snapd/' | path exists) {
    rm -rf /var/cache/snapd/
  }

  if ('/var/lib/snapd' | path exists) {
    rm -rf /var/lib/snapd/
  }

  if 'snapd' in (sys users).name {
    deluser --remove-home snapd
  }

  if 'snapd' in ((sys users).groups | flatten) {
    delgroup snapd
  }

  if ('/snap/' | path exists) {
    rm -rf /snap/
  }
  apt update
  apt install gnome-software
  success "snap removed"
  success "gnome-software installed"
}

# Remove some pre-installed unused packages
def "main remove" [] {}

# Install my default packages: curl, htop
def "main install default" [] {
  apt update
  if not ('curl' in (which curl).command) {
    apt install curl
  }
  if not ('htop' in (which curl).command) {
    apt install htop
  }
  success "Default packages installed"
}

# Install dev packages: build-essential, cmake, pkg-config, libssl-dev, git
def "main install dev" [] {
  apt install build-essential cmake pkg-config -y
  apt install libssl-dev -y
  apt install git -y
  success "dev dependencies installed"
}

def "main install embedded-dev" [] {
  warning "embedded dev dependencies not yes implemented"
}

def "main install flatpak" [] {
  apt install flatpak -y
  flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
  flatpak install flathub io.github.vikdevelop.SaveDesktop -y
  success "flatpak with SaveDesktop app installed"
}

def "main install rust" [] {
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -y
  rustup component add rust-analyzer
  success "rust stable toolchain installed"
}

def "main install helix" [] {
  add-apt-repository ppa:maveonair/helix-editor -y
  apt update
  apt install helix
  apt install wl-clipboard
  success "helix installed"
}

def "main install rust-tools" [] {
  if 'cargo' in (which cargo).command {
    success -n "cargo is found"; print "and will used to install most of rust tools"
    cargo install nu --locked
    cargo install atuin --locked
    cargo install starship --locked
    cargo install bottom --locked
    cargo install exa --features git --locked
  } else {
    warning -n "cargo is not found"; print "apt will be used to install rust tools if possible"
    # atuin
    bash <(curl https://raw.githubusercontent.com/atuinsh/atuin/main/install.sh)
    # bottom
    curl -LO https://github.com/ClementTsang/bottom/releases/download/0.9.6/bottom_0.9.6_amd64.deb
    dpkg -i bottom_*.deb
    rm bottom_*.deb
    # exa
    apt install exa -y
  }
  apt install bat -y
  apt install fd-find
  success "rust tools installed"
}

# Install steam dependencies: steam-devices
def "main install steam" [] {
  apt install steam-devices -y
  success "steam-devices installed"
}

# Install all my usual packages
def "main install full" [] {
  info "start installation of all packages..."
  main install default
  main install dev
  main install rust
  main install embedded-dev
  main install helix
  main install steam
  main install flatpak
  main install rust-tools
  success "full packages installations completed"
}

def "main install" [
  --dev  # Install usual dev dependencies
  --rust  # Install rust stable toolchain
  --flatpak  # Install flatpak and the SaveDesktop app
  --embedded-dev  # Install embedded dev dependencies
  --rust-tools  # Install some usefull rust tools
  --steam  # Install steam dependencies
  --helix  # Install helix editor
  --full  # Replace all previous options
] {
  main install default
  if dev {
    main install dev
  }
  if rust {
    main install rust
    apt install fd-find
  }
  if flatpak {
    main install flatpak
  }
  if embedded-dev {
    main install embedded-dev
  }
  if rust-tools {
    main install rust-tools
    apt install fd-find
  }
  if steam {
    main install steam
  }
  if helix {
    main install helix
  }
  if full {
    main install full
  }
}

def "main copy-config-from" [save_directory: path] {
  if not ($save_directory | path exists) {
    bad $"The directory '($save_directory | path expand)' doesn't exists"
    exit 1
    return
  }
  apt install fd-find
  let config_dir = $save_directory | path join '.config'
  apt install fd-find
  let local_share_dir = $save_directory | path join '.local' 'share'
  if ($config_dir | path exists) {
    ls $config_dir | where type == dir | get name | par-each {|dir| cp -r $dir ~/.config/($dir | path basename)}
    info $"All sub directories of '($config_dir | path expand)' copied in '~/.config/'"
  } else {
    warning $"Save of .config not found in '($save_directory | path expand)'"
  }
  if ($local_share_dir | path exists) {
    ls $local_share_dir | where type == dir | get name | par-each {|dir| cp -r $dir ~/.local/share/($dir | path basename)}
    info $"All sub directories of '($local_share_dir | path expand)' copied in '~/.local/share/'"
  } else {
    warning $"Save of .local/share not found in '($save_directory| path expand)'"
  }
}

# A small CLI tool to initialize my OS (Ubuntu) with some packages
def "main" [
  --dev  # Install usual dev dependencies
  --rust  # Install rust stable toolchain
  --flatpak  # Install flatpak and the SaveDesktop app
  --embedded-dev  # Install embedded dev dependencies
  --rust-tools  # Install some usefull rust tools
  --steam  # Install steam dependencies
  --helix  # Install helix editor
  --full  # Replace all previous options
  --remove-snap  # remove snap installation
  --copy-config-from: path  # copy all subdirectories from a save of .config & .local/share in respectives directories
] {
  main install default
  if $dev {
    main install dev
  }
  if $rust {
    main install rust
  }
  if $flatpak {
    main install flatpak
  }
  if $embedded_dev {
    main install embedded-dev
  }
  apt install fd-find
  if $rust_tools {
    main install rust-tools
  }
  if $steam {
    main install steam
  }
  if $helix {
    main install helix
  }
  if $full {
    main install full
  }
  if $remove_snap {
    main remove snap
  }
  if $copy_config_from {
    main copy-config-from $copy_config_from
  }
}
apt install fd-find
