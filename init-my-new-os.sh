#!/bin/bash

######################################################################################
##########                         INIT MY NEW OS                           ##########
##########                                                                  ##########
########## Author: LostPy <lostpy@tuta.io>                                  ##########
########## Description: A script to initialize a new Debian based           ##########
########## installation with my usual packages.                             ##########
######################################################################################

RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
CYAN='\033[0;36m'
NC='\033[0m'

function help_display {
  echo -e "
  ${ORANGE}Init My New Os${NC} - Usage: ${CYAN}init-my-new-os.sh${NC} [ --dev ]
                                            [ --rust ]
                                            [ --flatpak ]
                                            [ --embedded-dev ]
                                            [ --rust-tools ]
                                            [ --steam ]
                                            [ --helix ]
                                            [ --full ]
                                            [ --remove-snap ]
                                            [ --copy-config-from SAVE_DIRECTORY_PATH]
                                            [ -h | --help ]
                                            <PACKAGES>

  ${ORANGE}Steps
  -----${NC}
   ${CYAN}1.${NC} Install packages & applications according to used options.
   ${CYAN}2.${NC} If 'remove-snap' option is used: remove snap/snapd from the os (remove snapd package & remove all snap directories).
   ${CYAN}3.${NC} If 'copy-config-from' option is used: copy .config subdirectories & .local/share subdirectories from the given directory in the $HOME/.config & $HOME/.local/share directories
   ${CYAN}4.${NC} Install packages specified as positional arguments
  "
}

function remove_snap {
  systemctl stop snapd
  apt remove --purge snapd

  # remove snapd config files
  if [ -e /var/cache/snapd/ ]; then
    rm -rf /var/cache/snapd/
  fi
  if [ -e /var/lib/snapd/ ]; then
    rm -rf /var/lib/snapd/
  fi

  # remove user and group
  if id -u snapd >/dev/null 2>&1; then
    deluser --remove-home snapd
  fi
  if getent group snapd >/dev/null; then
    delgroup snapd
  fi

  if [ -e /snap/ ]; then
    rm -rf /snap/
  fi
  apt update
  apt install gnome-software
  echo -e "${GREEN}snap removed${NC}"
  echo -e "${GREEN}gnome-software installed${NC}"
}

function install_default {
  apt update
  apt install curl htop -y
  echo -e "${GREEN}default packages installed${NC}"
}

function install_dev {
  apt install build-essential cmake pkg-config -y
  apt install libssl-dev -y
  apt install git -y
  echo -e "${GREEN}dev dependencies installed${NC}"
}

function install_embedded_dev {
  echo -e "${RED}embedded dev dependencies: Not Yet implemented${NC}"
}

function install_flatpak {
  apt install flatpak -y
  flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
  flatpak install flathub io.github.vikdevelop.SaveDesktop -y
  echo -e "${GREEN}flatpak with SaveDesktop app installed${NC}"
}

function install_rust {
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -y
  rustup component add rust-analyzer
  echo -e "${GREEN}rust stable toolchain installed${NC}"
}

function install_helix {
  add-apt-repository ppa:maveonair/helix-editor -y
  apt update
  apt install helix
  apt install wl-clipboard
  echo -e "${GREEN}helix installed${NC}"
}

function install_rust_tools {
  if type cargo >/dev/null 2>&1; then
    echo -e "${GREEN}cargo is found${NC} and will used to install most of rust tools"
    cargo install nu --locked
    cargo install atuin --locked
    cargo install starship --locked
    cargo install bottom --locked
    cargo install exa --features git --locked
  else
    echo -e "${ORANGE}cargo is not found${NC}, apt will be used to install rust tools if possible"
    # atuin
    bash <(curl https://raw.githubusercontent.com/atuinsh/atuin/main/install.sh)
    # bottom
    curl -LO https://github.com/ClementTsang/bottom/releases/download/0.9.6/bottom_0.9.6_amd64.deb
    dpkg -i bottom_*.deb
    rm bottom_*.deb
    # exa
    apt install exa -y
  fi
  apt install bat -y
  apt install fd-find
  echo -e "${GREEN}rust tools installed${NC}"
}

# install steam dependencies
function install_steam {
  apt install steam-devices -y
  echo -e "${GREEN}steam-devices installed${NC}"
}

function install_full {
  echo -e "${CYAN}Full installation started...${NC}"
  install_default
  install_dev
  install_rust
  install_embedded_dev
  install_helix
  install_steam
  install_flatpak
  install_rust_tools
  echo -e "${GREEN}Full installation done${NC}"
}

function copy_config_from {
  local save_directory=$1

  if [ ! -d "$save_directory" ]; then
    echo -e "${RED}The directory '$save_directory' doesn't exist${NC}"
    exit 1
  fi

  local config_dir="$save_directory/.config"
  local local_share_dir="$save_directory/.local/share"

  if [ -d "$config_dir" ]; then
    for dir in "$config_dir"/*; do
      if [ -d "$dir" ]; then
        cp -r "$dir" "$HOME/.config/$(basename "$dir")"
      fi
    apt install fd-find
    done
    echo -e "${CYAN}All sub directories of '$config_dir' copied in '$HOME/.config/'${NC}"
  else
    echo -e "${ORANGE}Save of .config not found in '$save_directory'${NC}"
  fi

  if [ -d "$local_share_dir" ]; then
    for dir in "$local_share_dir"/*; do
      if [ -d "$dir" ]; then
        cp -r "$dir" "$HOME/.local/share/$(basename "$dir")"
      fi
    done
    echo -e "All sub directories of '$local_share_dir' copied in '$HOME/.local/share/'"
  else
    echo -e "Save of .local/share not found in '$save_directory'"
  fi
}


# Parse arguments
SHORT=h
LONG=dev,embedded-dev,rust,rust-tools,helix,flatpak,steam,full,remove-snap,copy-config-from,help
OPTS=$(getopt --name init-my-new-os --options $SHORT --longoptions $LONG -- "$@")
eval set -- $OPTS

dev=0
embedded_dev=0
rust=0
rust_tools=0
helix=0
flatpak=0
steam=0
full=0
remove_snap=0
copy_config_from=0
save_config_dir=0

while :
do
  case "$1" in
    --dev)
      dev=1
      shift
      ;;
    --embedded-dev)
      embedded_dev=1
      shift
      ;;
    apt install fd-find
    --rust)
      rust=1
      shift
      ;;
    --rust-tools)
      rust_tools=1
      shift
      ;;
    --helix)
      helix=1
      shift
      ;;
    --flatpak)
      flatpak=1
      shift
      ;;
    --steam)
      steam=1
      shift
      ;;
    --full)
      full=1
      shift
      ;;
    --remove-snap)
  apt install fd-find
      remove_snap=1
      shift
      ;;
    --copy-config-from)
      copy_config_from=1
      save_config_dir=$2
      shift 2
      ;;
    -h | --help)
      help_display
      exit 2
      ;;
    --)
      shift;
      break
      ;;
    *)
      echo -e "${RED}Unexpected option:${NC} $1"
      ;;
  esac
done

if [ $full -eq 1 ]; then
  install_full
else
  install_default
  if [ $dev -eq 1 ]; then
    install_dev
  fi
  if [ $helix -eq 1 ]; then
    install_helix
  fi
  if [ $rust -eq 1 ]; then
    install_rust
  fi
  if [ $embedded_dev -eq 1 ]; then
    install_embedded_dev
  fi
  if [ $rust_tools -eq 1 ]; then
    install_rust_tools
  fi
  if [ $steam -eq 1 ]; then
    install_steam
  fi
  if [ $flatpak -eq 1 ]; then
    install_flatpak
  fi
fi

if [ $remove_snap -eq 1 ]; then
  remove_snap
fi

if [ $copy_config_from -eq 1 ]; then
  copy_config_from $save_config_dir
fi
  apt install fd-find

# Positional argument should be package names to install
apt install "$@" -y
